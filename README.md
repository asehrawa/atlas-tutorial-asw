# MC Sample Generation

https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/mc_generation/

mkdir MCTutorial

cd MCTutorial

mkdir MCGeneration

cd MCGeneration

setupATLAS -c centos7

asetup AthGeneration,23.6.18

mkdir 1000000

cd 1000000

emacs mc.MGPy8EG_A14N23LO_LO_LQ_S1_PairProd_SameFlav_m1000.py

Gen_tf.py --ecmEnergy=13600 \
          --maxEvents=100 \
          --randomSeed=123456 \
          --outputEVNTFile=evgen.root \
          --jobConfig=1000000


# Producing truth deviations

https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/mc_truth_derivation/

cd MCTutorial

mkdir MCDerivation

cd MCDerivation

setupATLAS

asetup Athena,24.0.24

Derivation_tf.py --inputEVNTFile=../MCGeneration/evgen.root \
                 --outputDAODFile=mcval.pool.root \
                 --formats TRUTH1

checkxAOD.py DAOD_TRUTH1.mcval.pool.root


# MC Signal Validation

https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/mc_validation/


git clone https://gitlab.cern.ch/atlas-analysis-sw-tutorial/MCValidation.git

cd MCValidation

source setup.sh

source compile.sh

source build/x86_64*/setup.sh

cd run

source run.sh


# Output histograms

submitDir/hist-MCDerivation.root























































































