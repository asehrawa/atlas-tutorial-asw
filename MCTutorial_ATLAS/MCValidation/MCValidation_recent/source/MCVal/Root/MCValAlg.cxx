#include <AsgMessaging/MessageCheck.h>
#include <MCVal/MCValAlg.h>



MCValAlg :: MCValAlg (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty( "pdgIdBSM", m_pdgIdBSM = 36,  "Truth BSM PDGId" );

}



StatusCode MCValAlg :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // Create output tree
  ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
  TTree* mytree = tree ("analysis");

  // Declare branches
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);

  m_bsm_pt = new std::vector<float>();
  m_bsm_eta = new std::vector<float>();
  m_bsm_phi = new std::vector<float>();
  m_bsm_m = new std::vector<float>();
  m_bsm_pdgid = new std::vector<int>();

  mytree->Branch ("bsm_pt", &m_bsm_pt);
  mytree->Branch ("bsm_eta", &m_bsm_eta);
  mytree->Branch ("bsm_phi", &m_bsm_phi);
  mytree->Branch ("bsm_m", &m_bsm_m);
  mytree->Branch ("bsm_pdgid", &m_bsm_pdgid);
  mytree->Branch ("bsm_N", &m_bsm_N);

  m_lep_pt = new std::vector<float>();
  m_lep_eta = new std::vector<float>();
  m_lep_phi = new std::vector<float>();
  m_lep_m = new std::vector<float>();
  m_lep_pdgid = new std::vector<int>();

  mytree->Branch ("lep_pt", &m_lep_pt);
  mytree->Branch ("lep_eta", &m_lep_eta);
  mytree->Branch ("lep_phi", &m_lep_phi);
  mytree->Branch ("lep_m", &m_lep_m);
  mytree->Branch ("lep_pdgid", &m_lep_pdgid);
  mytree->Branch ("lep_N", &m_lep_N);

  m_el_pt = new std::vector<float>();
  m_el_eta = new std::vector<float>();
  m_el_phi = new std::vector<float>();
  m_el_m = new std::vector<float>();

  mytree->Branch ("el_pt", &m_el_pt);
  mytree->Branch ("el_eta", &m_el_eta);
  mytree->Branch ("el_phi", &m_el_phi);
  mytree->Branch ("el_m", &m_el_m);
  mytree->Branch ("el_N", &m_el_N);

  m_mu_pt = new std::vector<float>();
  m_mu_eta = new std::vector<float>();
  m_mu_phi = new std::vector<float>();
  m_mu_m = new std::vector<float>();

  mytree->Branch ("mu_pt", &m_mu_pt);
  mytree->Branch ("mu_eta", &m_mu_eta);
  mytree->Branch ("mu_phi", &m_mu_phi);
  mytree->Branch ("mu_m", &m_mu_m);
  mytree->Branch ("mu_N", &m_mu_N);

  m_q_pt = new std::vector<float>();
  m_q_eta = new std::vector<float>();
  m_q_phi = new std::vector<float>();
  m_q_m = new std::vector<float>();
  m_q_pdgid = new std::vector<int>();

  mytree->Branch ("q_pt", &m_q_pt);
  mytree->Branch ("q_eta", &m_q_eta);
  mytree->Branch ("q_phi", &m_q_phi);
  mytree->Branch ("q_m", &m_q_m);
  mytree->Branch ("q_pdgid", &m_q_pdgid);
  mytree->Branch ("q_N", &m_q_N);

  m_jet_pt = new std::vector<float>();
  m_jet_eta = new std::vector<float>();
  m_jet_phi = new std::vector<float>();
  m_jet_m = new std::vector<float>();

  mytree->Branch ("jet_pt", &m_jet_pt);
  mytree->Branch ("jet_eta", &m_jet_eta);
  mytree->Branch ("jet_phi", &m_jet_phi);
  mytree->Branch ("jet_m", &m_jet_m);
  mytree->Branch ("jet_N", &m_jet_N);

  mytree->Branch ("met", &m_met);
  mytree->Branch ("met_phi", &m_met_phi);
  
  // Book output histograms
  ANA_CHECK( book( TH1F("h_N_bsm","h_N_bsm",5,-0.5,4.5) ) );
  ANA_CHECK( book( TH1F("h_m_bsm","h_m_bsm",100,0,2000) ) );
  
  ANA_CHECK( book( TH1F("h_N_el","h_N_el",5,-0.5,4.5) ) );
  ANA_CHECK( book( TH1F("h_pt_el","h_pt_el",50,0,1000) ) );
  ANA_CHECK( book( TH1F("h_eta_el","h_eta_el",40,-4.0,4.0) ) );
  ANA_CHECK( book( TH1F("h_phi_el","h_phi_el",32,-3.2,3.2) ) );
  
  ANA_CHECK( book( TH1F("h_N_mu","h_N_mu",5,-0.5,4.5) ) );
  ANA_CHECK( book( TH1F("h_pt_mu","h_pt_mu",50,0,1000) ) );
  ANA_CHECK( book( TH1F("h_eta_mu","h_eta_mu",40,-4.0,4.0) ) );
  ANA_CHECK( book( TH1F("h_phi_mu","h_phi_mu",32,-3.2,3.2) ) );
  
  ANA_CHECK( book( TH2F("h_N_el_vs_N_mu","h_N_el_vs_N_mu",5,-0.5,4.5,5,-0.5,4.5) ) );
  
  ANA_CHECK( book( TH1F("h_N_lep","h_N_lep",5,-0.5,4.5) ) );
  ANA_CHECK( book( TH1F("h_pdgid_lep","h_pdgid_lep",32,-16.5,16.5) ) );
  ANA_CHECK( book( TH1F("h_pt_lep","h_pt_lep",50,0,1000) ) );
  ANA_CHECK( book( TH1F("h_eta_lep","h_eta_lep",40,-4.0,4.0) ) );
  ANA_CHECK( book( TH1F("h_phi_lep","h_phi_lep",32,-3.2,3.2) ) );
  
  ANA_CHECK( book( TH1F("h_N_q","h_N_q",8,-0.5,7.5) ) );
  ANA_CHECK( book( TH1F("h_pt_q","h_pt_q",50,0,1000) ) );
  ANA_CHECK( book( TH1F("h_eta_q","h_eta_q",40,-4.0,4.0) ) );
  ANA_CHECK( book( TH1F("h_phi_q","h_phi_q",32,-3.2,3.2) ) );
  ANA_CHECK( book( TH1F("h_pdgid_q","h_pdgid_q",14,-7.5,7.5) ) );
  
  ANA_CHECK( book( TH1F("h_N_jet","h_N_jet",20,-0.5,19.5) ) );
  ANA_CHECK( book( TH1F("h_pt_jet","h_pt_jet",50,0,1000) ) );
  ANA_CHECK( book( TH1F("h_eta_jet","h_eta_jet",40,-4.0,4.0) ) );
  ANA_CHECK( book( TH1F("h_phi_jet","h_phi_jet",32,-3.2,3.2) ) );
  
  ANA_CHECK( book( TH1F("h_met","h_met",50,0,1000) ) );
  ANA_CHECK( book( TH1F("h_met_phi","h_met_phi",32,-3.2,3.2) ) );
  
  return StatusCode::SUCCESS;
}



StatusCode MCValAlg :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // Truth particles
  const xAOD::TruthParticleContainer* truth_particles = nullptr;
  ANA_CHECK ( evtStore()->retrieve(truth_particles, "TruthParticles" ) );

  std::vector<const xAOD::TruthParticle*> vec_bsm;  
  std::vector<const xAOD::TruthParticle*> vec_lep;
  std::vector<const xAOD::TruthParticle*> vec_nu;
  std::vector<const xAOD::TruthParticle*> vec_el;
  std::vector<const xAOD::TruthParticle*> vec_mu;
  std::vector<const xAOD::TruthParticle*> vec_q;

  for(auto tp : *truth_particles){

    // Skip particles that come from hadron decays
    if(!Truth::notFromHadron(tp)) continue;

    // Skip gluons
    if(isGluon(tp->pdgId())) continue;

    // BSM particles
    if(std::abs(tp->pdgId()) == m_pdgIdBSM) {
      // Skip if it is a self-decay
      if(tp->parent()->pdgId() == tp->pdgId()) continue;

      // Store BSM particle
      vec_bsm.push_back(tp);
    }

    if(Truth::isFinalElectron(tp) && !Truth::isFromPhoton(tp) && Truth::isFromParticle(tp,m_pdgIdBSM)) {
      vec_el.push_back(tp);
      vec_lep.push_back(tp);
    }
  
    if(Truth::isFinalMuon(tp) && !Truth::isFromPhoton(tp) && Truth::isFromParticle(tp,m_pdgIdBSM)) {
      vec_mu.push_back(tp);
      vec_lep.push_back(tp);
    }

    if(Truth::isFinalQuark(tp) && !Truth::isFromGluon(tp) && Truth::isFromParticle(tp,m_pdgIdBSM)) {
      if(std::abs(tp->pdgId())!=5) {
        vec_q.push_back(tp);
      }
    }

  }

  // Clear BSM vectors
  m_bsm_pt->clear();
  m_bsm_eta->clear();
  m_bsm_phi->clear();
  m_bsm_m->clear();
  m_bsm_pdgid->clear();

  // Store number of BSM particles
  hist("h_N_bsm")->Fill(vec_bsm.size());
  m_bsm_N = vec_bsm.size();

  // Loop over BSM particles
  for(auto bsm : vec_bsm) {
    // Fill histograms
    hist("h_m_bsm")->Fill(bsm->m()/Truth::GeV);
    // Fill vectors for branches
    m_bsm_pt->push_back(bsm->pt());
    m_bsm_eta->push_back(bsm->eta());
    m_bsm_phi->push_back(bsm->phi());
    m_bsm_m->push_back(bsm->m());
    m_bsm_pdgid->push_back(bsm->pdgId());
  }
 
  // Clear electron vectors
  m_el_pt->clear();
  m_el_eta->clear();
  m_el_phi->clear();
  m_el_m->clear();

  // Store number of electrons
  hist("h_N_el")->Fill(vec_el.size());
  m_el_N = vec_el.size();

  // Loop over electrons
  for(auto el : vec_el) {
    // Fill histograms
    hist("h_pt_el")->Fill(el->pt()/Truth::GeV);
    hist("h_eta_el")->Fill(el->eta());
    hist("h_phi_el")->Fill(el->phi());
    // Fill vectors for branches
    m_el_pt->push_back(el->pt());
    m_el_eta->push_back(el->eta());
    m_el_phi->push_back(el->phi());
    m_el_m->push_back(el->m());
  }
 
  // Clear muon vectors
  m_mu_pt->clear();
  m_mu_eta->clear();
  m_mu_phi->clear();
  m_mu_m->clear();

  // Store number of muons
  hist("h_N_mu")->Fill(vec_mu.size());
  m_mu_N = vec_mu.size();

  // Loop over muons
  for(auto mu : vec_mu) {
    // Fill histograms
    hist("h_pt_mu")->Fill(mu->pt()/Truth::GeV);
    hist("h_eta_mu")->Fill(mu->eta());
    hist("h_phi_mu")->Fill(mu->phi());
    // Fill vectors for branches
    m_mu_pt->push_back(mu->pt());
    m_mu_eta->push_back(mu->eta());
    m_mu_phi->push_back(mu->phi());
    m_mu_m->push_back(mu->m());
  }

  // Fill 2D lepton multiplicity histogram
  hist("h_N_el_vs_N_mu")->Fill(vec_el.size(),vec_mu.size());
  
  // Clear lepton vectors
  m_lep_pt->clear();
  m_lep_eta->clear();
  m_lep_phi->clear();
  m_lep_m->clear();
  m_lep_pdgid->clear();
  
  // Store number of leptons
  hist("h_N_lep")->Fill(vec_lep.size());
  m_lep_N = vec_lep.size();

  // Loop over leptons
  for(auto lep : vec_lep) {
    // Fill histograms
    hist("h_pdgid_lep")->Fill(lep->pdgId());
    hist("h_pt_lep")->Fill(lep->pt()/Truth::GeV);
    hist("h_eta_lep")->Fill(lep->eta());
    hist("h_phi_lep")->Fill(lep->phi());
    // Fill vectors for branches
    m_lep_pt->push_back(lep->pt());
    m_lep_eta->push_back(lep->eta());
    m_lep_phi->push_back(lep->phi());
    m_lep_m->push_back(lep->m());
    m_lep_pdgid->push_back(lep->pdgId());
  }

  // Clear quark vectors
  m_q_pt->clear();
  m_q_eta->clear();
  m_q_phi->clear();
  m_q_m->clear();
  m_q_pdgid->clear();

  // Store number of quarks
  hist("h_N_q")->Fill(vec_q.size());
  m_q_N = vec_q.size();

  // Loop over quarks
  for(auto q : vec_q) {
    // Fill histograms
    hist("h_pdgid_q")->Fill(q->pdgId());
    hist("h_pt_q")->Fill(q->pt()/Truth::GeV);
    hist("h_eta_q")->Fill(q->eta());
    hist("h_phi_q")->Fill(q->phi());
    // Fill vectors for branches
    m_q_pt->push_back(q->pt());
    m_q_eta->push_back(q->eta());
    m_q_phi->push_back(q->phi());
    m_q_m->push_back(q->m());
    m_q_pdgid->push_back(q->pdgId());
  }

  // Jets
  const xAOD::JetContainer* truth_jets = nullptr;
  ANA_CHECK ( evtStore()->retrieve(truth_jets, "AntiKt4TruthDressedWZJets" ) );

  m_jet_pt->clear();
  m_jet_eta->clear();
  m_jet_phi->clear();
  m_jet_m->clear();

  int nJets = 0;
  for(auto jet : *truth_jets) {
    // Skip jets with pt < 20 GeV
    if(jet->pt()/Truth::GeV < 20.) continue;

    m_jet_pt->push_back(jet->pt());
    m_jet_eta->push_back(jet->eta());
    m_jet_phi->push_back(jet->phi());
    m_jet_m->push_back(jet->m());

    hist("h_pt_jet")->Fill(jet->pt()/Truth::GeV);
    hist("h_eta_jet")->Fill(jet->eta());
    hist("h_phi_jet")->Fill(jet->phi());

    nJets++;
  }
  hist("h_N_jet")->Fill(nJets);
  m_jet_N = nJets;

  // MET
  const xAOD::MissingETContainer* truth_met = nullptr;
  ANA_CHECK ( evtStore()->retrieve(truth_met, "MET_Truth" ) );

  for(auto met : *truth_met) {
    if(met->name()=="NonInt") {
      m_met = met->met();
      m_met_phi = met->phi();
    }
  }

  hist("h_met")->Fill(m_met/Truth::GeV);
  hist("h_met_phi")->Fill(m_met_phi);

  tree ("analysis")->Fill ();

  return StatusCode::SUCCESS;
}



StatusCode MCValAlg :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

MCValAlg :: ~MCValAlg () {

  // Delete the allocated vectors to avoid memory leaks
  delete m_bsm_pt;
  delete m_bsm_eta;
  delete m_bsm_phi;
  delete m_bsm_m;
  delete m_bsm_pdgid;

  delete m_el_pt;
  delete m_el_eta;
  delete m_el_phi;
  delete m_el_m;

  delete m_mu_pt;
  delete m_mu_eta;
  delete m_mu_phi;
  delete m_mu_m;

  delete m_q_pt;
  delete m_q_eta;
  delete m_q_phi;
  delete m_q_m;
  delete m_q_pdgid;

  delete m_jet_pt;
  delete m_jet_eta;
  delete m_jet_phi;
  delete m_jet_m;

}

