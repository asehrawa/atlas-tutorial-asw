import ROOT
import os

# Open the ROOT file
file = ROOT.TFile("hist-MCValidation.root")

# Directory to save the plots
output_dir = "/eos/user/a/asehrawa/Validation-test-ATLAS/"

# Ensure the output directory exists
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# List of histogram names
histogram_names = [
    "h_N_bsm", "h_m_bsm", "h_N_el", "h_pt_el", "h_eta_el", "h_phi_el",
    "h_N_mu", "h_pt_mu", "h_eta_mu", "h_phi_mu", "h_N_el_vs_N_mu",
    "h_N_lep", "h_pdgid_lep", "h_pt_lep", "h_eta_lep", "h_phi_lep",
    "h_N_q", "h_pt_q", "h_eta_q", "h_phi_q", "h_pdgid_q", "h_N_jet",
    "h_pt_jet", "h_eta_jet", "h_phi_jet", "h_met", "h_met_phi"
]

# Loop through the histogram names and plot each on a different canvas
for hist_name in histogram_names:
    hist = file.Get(hist_name)
    if not hist:
        print(f"Histogram {hist_name} not found in the file.")
        continue
    
    # Create a canvas
    canvas = ROOT.TCanvas(f"canvas_{hist_name}", hist_name, 800, 600)
    
    # Draw the histogram
    hist.Draw()
    
    # Save the canvas as an image file
    canvas.SaveAs(os.path.join(output_dir, f"{hist_name}.png"))

# Close the ROOT file
file.Close()
