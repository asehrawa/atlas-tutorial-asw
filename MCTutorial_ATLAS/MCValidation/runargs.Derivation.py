#!/usr/bin/env athena.py --CA
# Run arguments file auto-generated on Thu May 23 18:42:15 2024 by:
# JobTransform: Derivation
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'Derivation' 

runArgs.perfmon = 'fastmonmt'
runArgs.formats = ['TRUTH1']

# Explicitly added to process all events in this step
runArgs.maxEvents = -1

 # Input data
runArgs.inputEVNTFile = ['evgen.root']
runArgs.inputEVNTFileType = 'EVNT'
runArgs.inputEVNTFileNentries = 100
runArgs.EVNTFileIO = 'input'

 # Output data
runArgs.outputDAOD_TRUTH1File = 'DAOD_TRUTH1.mcval.pool.root'
runArgs.outputDAOD_TRUTH1FileType = 'AOD'

 # Extra runargs

 # Extra runtime runargs

 # Literal runargs snippets

 # Executor flags
runArgs.totalExecutorSteps = 0

 # Threading flags
runArgs.nprocs = 0
runArgs.threads = 0
runArgs.concurrentEvents = 0

 # Import skeleton and execute it
from DerivationFrameworkConfiguration.DerivationSkeleton import fromRunArgs
fromRunArgs(runArgs)
