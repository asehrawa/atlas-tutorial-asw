# Run arguments file auto-generated on Thu May 23 18:23:40 2024 by:
# JobTransform: generate
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'generate' 

runArgs.perfmon = 'fastmonmt'
runArgs.ecmEnergy = 13600.0
runArgs.firstEvent = 1
runArgs.randomSeed = 123456
runArgs.printEvts = 5
runArgs.generatorRunMode = 'run'
runArgs.generatorJobNumber = 0
runArgs.lheOnly = 0
runArgs.cleanOut = 0
runArgs.VERBOSE = False
runArgs.ignoreBlackList = False
runArgs.maxEvents = 100
runArgs.jobConfig = ['1000000']

 # Input data

 # Output data
runArgs.outputEVNTFile = 'evgen.root'
runArgs.outputEVNTFileType = 'EVNT'

 # Extra runargs

 # Extra runtime runargs

 # Literal runargs snippets

 # Executor flags
runArgs.totalExecutorSteps = 0
