# Import all of the necessary methods to use MadGraph
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Mass of the leptoquark
lq_mass = 500. # GeV

# Number of events to produce
safety = 1.1 # safety factor to account for filter efficiency
nevents = runArgs.maxEvents * safety

# Make sure LQ PDG IDs are known to TestHepMC:
pdgfile = open("pdgid_extras.txt", "w+")
pdgfile.write("""
-9000005
9000005
""")
pdgfile.close()

# Here is where we define the commands that will be passed to MadGraph

# Import the LQ model
process = """
import model LO_LQ_S1
"""

# Define some multi-particle represenations
process += """
define charm = c c~
define up = u u~
define q = u u~ d d~ c c~ s s~
define e = e- e+
define mu = mu- mu+
"""

# Define the physics process to be simulated
process += """
generate g g > e mu up charm
"""

# This defines the MadGraph outputs
process += """
output -f
"""

# Define the process and create the run card from a template
process_dir = new_process(process)
settings = {'ickkw': 0, 'nevents':nevents}
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# Set some values in the param card
# BSM particle masses
masses={'9000005':lq_mass, #S1
        '1000021':1000000. } # chi10 - needed because of a bug in the model

# Leptoquark width
# This is hard-coded here, but could be calculated on the fly with a function
lq_width = 39.7887
decays={'9000005':"""DECAY 9000005  %g #leptoquark decay""" % lq_width}

# These are the couplings of the leptoquarks to first and second
# generation fermions
yuks1ll={'1   1':"""0.000000e-00 # yll1x1"""}
yuks1rr={'1   1':"""1.000000e-01 # yRR1x1"""}
yuks1rr={'2   2':"""1.000000e-01 # yRR2x2"""}

# Create the param card and modify some parameters from their default values
modify_param_card(process_dir=process_dir,params={'MASS':masses,'DECAY':decays,'YUKS1LL':yuks1ll,'YUKS1RR':yuks1rr})

# Do the event generation
generate(process_dir=process_dir,runArgs=runArgs)

# These details are important information about the JOs
evgenConfig.description = 'Single Leptoquark coupling lam1122. m_S1 = %s GeV' % (lq_mass)
evgenConfig.contact = [ "Jason Veatch <jason.veatch@cern.ch>" ]
evgenConfig.keywords += ['BSM','exotic', 'scalar', 'leptoquark']

arrange_output(process_dir=process_dir, runArgs=runArgs)
