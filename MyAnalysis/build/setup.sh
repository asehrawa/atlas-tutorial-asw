#!/bin/bash
# This script sets up the environment for running MyAnalysis

export MYANALYSIS_ROOT=/srv/source
export PATH=/srv/build/bin:$PATH
