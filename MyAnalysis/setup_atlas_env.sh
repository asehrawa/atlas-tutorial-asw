#!/bin/bash

# Define the ATLAS Local Root Base
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase

# Function to set up ATLAS environment
setup_atlas_environment() {
    echo "Setting up ATLAS environment..."
    source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

    # Check if running on AlmaLinux 9 and set up CentOS 7 container if necessary
    if grep -q "AlmaLinux 9" /etc/os-release; then
        echo "AlmaLinux 9 detected. Setting up CentOS 7 container..."
        setupATLAS -c centos7
    fi


    
    
    # Load the specific ATLAS release, generators, and Athena
    echo "Loading ATLAS release and tools..."
    asetup AnalysisBase,25.2.2

    asetup Athena,master,latest
    # Check if .asetup.save exists and restore the environment if it does
    if [ -f .asetup.save ]; then
        echo "Restoring previous setup..."
        asetup --restore
    else
        echo ".asetup.save not found. Setting up a new environment..."
        # Add other setup commands as necessary
    fi

    # Load other necessary modules
    echo "Loading additional tools and modules..."
    lsetup "lcgenv -p LCG_96 x86_64-centos7-gcc8-opt Python"
    lsetup "lcgenv -p LCG_96 x86_64-centos7-gcc8-opt ROOT"

    echo "Environment setup complete."
}

# Main script execution
setup_atlas_environment

# Optionally, you can add more setup commands here if needed

# Print the current environment settings for verification
echo "Current Environment Settings:"
echo "ATLAS_LOCAL_ROOT_BASE: $ATLAS_LOCAL_ROOT_BASE"
echo "PATH: $PATH"
echo "LD_LIBRARY_PATH: $LD_LIBRARY_PATH"
echo "PYTHONPATH: $PYTHONPATH"

# Create a script to easily restore the environment
cat <<EOF > setup.sh
#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup --restore
EOF

chmod +x setup.sh

echo "Setup script created as setup.sh. You can use this script to restore the environment in new sessions."

# End of the script
