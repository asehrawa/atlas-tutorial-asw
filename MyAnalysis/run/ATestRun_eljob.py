import yaml
import sys
import os
import subprocess

# Print the current working directory
print("Current Working Directory:", os.getcwd())

# Add the full path to the python directory containing the MyAnalysis package
sys.path.append('/afs/cern.ch/user/a/asehrawa/MyAnalysis/source/python')

from MyAnalysis.MyAnalysisAlgorithms import makeSequence

def main(config_path, submission_dir):
    # Hard-coded input file path
    input_file = "/afs/cern.ch/user/a/asehrawa/MCTutorial_ATLAS/MCValidation/MCValidation/run/submitDir/hist-MCValidation.root"

    # Load configuration
    with open(config_path, 'r') as stream:
        config = yaml.safe_load(stream)

    print("Running job with the following configuration:")
    print(yaml.dump(config, default_flow_style=False))

    # Create an algorithm sequence from the YAML configuration file
    data_type = "mc"  # Set data type to MC
    algSeq = makeSequence(config_path, data_type)  # Only pass two arguments
    print(algSeq)  # For debugging

    # Simulate running the job
    subprocess.run(["./MyExecutable", input_file])

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Run analysis job")
    parser.add_argument("--config-path", required=True, help="Path to the YAML configuration file")
    parser.add_argument("--submission-dir", required=True, help="Directory to store submission files")
    args = parser.parse_args()

    os.makedirs(args.submission_dir, exist_ok=True)
    main(args.config_path, args.submission_dir)
