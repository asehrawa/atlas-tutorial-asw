#include <iostream>
#include "MyAlgorithm.h"

void MyAlgorithm::run() {
    std::cout << "Running MyAlgorithm with Muon Selection!" << std::endl;
}

int main() {
    MyAlgorithm alg;
    alg.run();
    return 0;
}
