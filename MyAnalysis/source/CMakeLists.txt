cmake_minimum_required(VERSION 3.5)
project(MyAnalysis)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

# Add executable
add_executable(MyExecutable MyAlgorithm.cpp)

# Install Python modules
install(DIRECTORY python/ DESTINATION ${CMAKE_INSTALL_PREFIX}/python)

# Generate the setup.sh script
configure_file(
    "${CMAKE_SOURCE_DIR}/setup.sh.in"
    "${CMAKE_BINARY_DIR}/setup.sh"
    @ONLY
)
