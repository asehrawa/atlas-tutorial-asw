import yaml
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def makeSequence(config_path, data_type):
    with open(config_path, 'r') as stream:
        config = yaml.safe_load(stream)

    acc = ComponentAccumulator()

    #if 'CommonServices' in config:
     #   from CPCommonModules.CPCommonModulesConfig import CommonServicesCfg
      #  acc.merge(CommonServicesCfg())

    #if 'CPAlgorithms' in config:
     #   from MuonAnalysisAlgorithms.MuonAnalysisConfig import MuonSelectionCfg
      #  muon_cfg = config['CPAlgorithms'].get('MuonSelection', {})
       # acc.merge(MuonSelectionCfg(muon_cfg.get('WorkingPoint', 'Medium')))

    # Add other CPAlgorithms configurations similarly...

    return acc
